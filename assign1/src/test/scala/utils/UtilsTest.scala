package test.scala.utils

import org.scalatest.FunSuite
import org.scalatest.prop.TableDrivenPropertyChecks
import org.scalatest.Matchers
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import main.scala.utils.Utils

@RunWith(classOf[JUnitRunner])
class UtilsTest extends FunSuite with Matchers with TableDrivenPropertyChecks {

  val testForGet2DArray = Table(
    ("output", "rows", "columns"),
    (Array(Array(false)), 1, 1),
    (Array(Array(false, false, false, false, false), Array(false, false, false, false, false), Array(false, false, false, false, false), Array(false, false, false, false, false), Array(false, false, false, false, false)), 5, 5));

  for ((output, rows, columns) <- testForGet2DArray) {
    test("test get2DArray for rows: " + rows + " columns: " + columns) {
      var uitls = new Utils();
      uitls.get2DArray(rows, columns) should equal(output);
    }
  }

  val testForChangeStateOfCell = Table(("cellsToChange", "state", "output"),
    (Array((0, 0)), Array(Array(false)), List(List(true))),
    (Array((0, 0), (1, 1), (2, 2)), Array(Array(false, false, false), Array(false, false, false), Array(false, false, false)), Array(Array(true, false, false), Array(false, true, false), Array(false, false, true))));

  for ((cellsToChange, state, output) <- testForChangeStateOfCell) {
    test("test changeStateOfCell for rows: " + cellsToChange + " columns: " + state) {
      var uitls = new Utils();
      uitls.changeStateOfCell(cellsToChange, state)
      state should equal(output);
    }
  }

}