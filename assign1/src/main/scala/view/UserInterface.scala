package main.scala.view

import scalafx.application.JFXApp
import scalafx.application.JFXApp.PrimaryStage
import scalafx.scene.Scene
import scalafx.scene.layout.BorderPane
import main.scala.DisplayControls
import scalafx.scene.layout.StackPane
import scala.collection.mutable.Map
import main.scala.utils.Constants
import main.scala.utils.Utils

class UserInterface(rows: Int, columns: Int, intialCells: Array[(Int, Int)]) extends JFXApp {
  var twoDarray = new Utils().get2DArray(rows, columns);
  stage = new PrimaryStage {
    title = "Game of Life";
    width = columns * 20;
    height = (rows + 1) * 20;
    scene = new Scene {
      root = new BorderPane {
        top = new DisplayControls().addHBox(columns * 20, 20);
        new Utils().changeStateOfCell(intialCells, twoDarray);
        center = new DisplayGrid().addGridPane(rows, columns, Map[String, StackPane](), twoDarray);
      }
    }
  }
}

