package main.scala.view

import scalafx.scene.layout.GridPane
import scalafx.scene.control.CheckBox
import scalafx.scene.layout.StackPane
import main.scala.utils.Constants
import scala.collection.mutable.Map

class DisplayGrid extends Constants {

  def addGridPane(rows: Int, columns: Int, paneMap: Map[String, StackPane], twoDArray: Array[Array[Boolean]]): GridPane = {
    new GridPane() {
      for (row <- 1 to rows) {
        for (column <- 1 to columns) {
          val pane = new StackPane() {
            prefHeight = 20;
            prefWidth = 20;
            layoutX = row * 20;
            layoutY = column * 20;
            if (twoDArray(row - 1)(column - 1)) {
              style = ALIVE_CELL;
            } else {
              style = DEAD_CELL;
            }
          }
          paneMap += { row + " " + column -> pane };
          add(pane, row, column);
        }
      }
    }
  }

}