package main.scala

import scalafx.Includes.handle
import scalafx.geometry.Insets
import scalafx.scene.control.Button
import scalafx.scene.control.Button.sfxButton2jfx
import scalafx.scene.layout.HBox

class DisplayControls {

  def addHBox(widht1: Int, height1: Int): HBox = {
    val start = new Button("Start") {
      onAction = handle { new HandleGameEvents().startEvent(); };
    }
    val end = new Button("End") {
      onAction = handle { new HandleGameEvents().endEvent(); };
    }
    val exit = new Button("Exit") {
      onAction = handle { new HandleGameEvents().exitEvent(); };
    }
    new HBox {
      padding = Insets(15, 12, 15, 12);
      spacing = 10;
      prefWidth = widht1;
      prefHeight= height1;
      style = "-fx-background-color: #336699;";
      children.addAll(start, end, exit);
    }
  }

}