package main.scala.utils

import scala.collection.immutable.Set

class Utils {

  def get2DArray(rows: Int, columns: Int): Array[Array[Boolean]] = {
    var array = Array.ofDim[Boolean](rows, columns);
    for (row <- 0 until rows) {
      for (column <- 0 until columns) {
        array(row)(column) = false;
      }
    }
    array
  }

  def changeStateOfCell(cellsToChange: Array[(Int, Int)], state: Array[Array[Boolean]]) {
    for (tuple <- cellsToChange) {
      state(tuple._1)(tuple._2) = true;
    }
  }

}