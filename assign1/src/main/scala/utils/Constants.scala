package main.scala.utils

class Constants {
  val DEAD_CELL = "-fx-background-color: white; -fx-border-color: black; -fx-border-width: 0.1;";
  val ALIVE_CELL = "-fx-background-color: blue; -fx-border-color: black; -fx-border-width: 0.1;";
}