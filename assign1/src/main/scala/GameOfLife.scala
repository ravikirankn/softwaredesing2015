package main.scala

import main.scala.view.UserInterface
import main.scala.utils.Utils

object GameOfLife {

  def main(args: Array[String]): Unit = {
    val rows = 20;
    val columns = 20;
    val intialCells = Array((0, 0), (0, 1), (0, 2));
    new UserInterface(rows, columns, intialCells).main(Array());
  }

}