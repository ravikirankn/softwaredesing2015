name := "GameOfLife"

version := "1.0"

scalaVersion := "2.11.6"

resolvers += Opts.resolver.sonatypeSnapshots

libraryDependencies += "org.scalafx" %% "scalafx" % "8.0.40-R9-SNAPSHOT"

fork := true
