package view

import scalafx.Includes._
import scalafx.event.ActionEvent
import scalafx.scene.control.{SeparatorMenuItem, MenuItem, Menu, MenuBar}

class DisplayControls(val UI: UserInterface) {
  def createMenuBar(): MenuBar ={
    val menu = new Menu("File") {
      items = List(
        new MenuItem {
          text = "New Game"
          onAction = handle { UI.showSizeDialog() }
        },
        new SeparatorMenuItem,
        new MenuItem {
          text = "Exit"
          onAction = handle { System.exit(0) }
        }
      )
    }
    new MenuBar {
      useSystemMenuBar = true
      minWidth = 100
      menus.add(menu)
      val os = System.getProperty ("os.name")
      if (os != null && os.startsWith ("Mac"))
        useSystemMenuBar = false
    }
  }
}
