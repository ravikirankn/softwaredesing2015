package view

import controller.GameStateController

import scalafx.Includes._
import scalafx.application.JFXApp
import scalafx.application.JFXApp.PrimaryStage
import scalafx.beans.property.BooleanProperty
import scalafx.event.ActionEvent
import scalafx.geometry.Insets
import scalafx.scene.Scene
import scalafx.scene.control.ButtonBar.ButtonData
import scalafx.scene.control._
import scalafx.scene.layout.{BorderPane, GridPane}
import scalafx.scene.paint.Color


class UserInterface(Controller: GameStateController) extends JFXApp {
  val displayControls = new DisplayControls(this)
  stage = new PrimaryStage {
    title = "Conway's Game of Life"
    width = 640
    height = 480
    scene = new Scene {
      fill = Color.LightGray
      root = new BorderPane {
        top = displayControls.createMenuBar()
        //center =
        //bottom =
      }
    }
  }


  def showSizeDialog(): Unit ={
    case class GridSize(height: Int, width: Int)

    val dialog = new Dialog[GridSize]() {
      initOwner(stage)
      title = "Grid Size"
      headerText = "Set the size of the grid"
    }
    // Set the button types.
    val saveButtonType = new ButtonType("Save", ButtonData.OKDone)
    dialog.dialogPane().buttonTypes = Seq(saveButtonType, ButtonType.Cancel)
    //fields for input
    val heightField = new TextField {
      promptText = "Height"
    }
    val widthField = new TextField {
      promptText = "Width"
    }
    //layout of dialog
    val grid = new GridPane() {
      hgap = 10
      vgap = 10
      padding = Insets(20, 10, 10, 10)
      add(new Label("Height:"), 0, 0)
      add(heightField, 1, 0)
      add(new Label("Width:"), 0, 1)
      add(widthField, 1, 1)
    }

    //assign layout to dialog
    dialog.dialogPane().content = grid
    //disable save button until value is written in 2nd field
    val saveButton = dialog.dialogPane().lookupButton(saveButtonType)
    saveButton.disable = true
    widthField.text.onChange { (_, _, newValue) => saveButton.disable = newValue.trim().isEmpty}

    dialog.resultConverter = dialogButton =>
      if (dialogButton == saveButtonType) GridSize(heightField.text().toInt, widthField.text().toInt)
      else null

    val result = dialog.showAndWait()
    result match {
      case Some(GridSize(h, w)) => {
        println("Height=" + h + ", Width=" + w)
        showCellsDialog(h, w)
      }
      case None => println("Dialog returned: None")
    }
  }


  def showCellsDialog(h: Int, w: Int): Unit ={
    val dialog = new Dialog[Array[Boolean]]() {
      initOwner(stage)
      title = "Initial Cells"
      headerText = "Check the cells that are alive"
    }
    //layout of dialog
    val cells = new Array[Boolean](h*w)
    val checkBoxes = Array.fill(h * w) {
      new CheckBox
    }

    val grid = new GridPane() {
      hgap = 10
      vgap = 10
      padding = Insets(10, 10, 10, 10)
      add(new Label("Cells:"), 0, 0)
      var index = 0
      for(y <- 1 to w){
        for(x <- 1 to h){
          add(checkBoxes(index), x, y)
          index+=1
        }
      }
    }
    dialog.dialogPane().buttonTypes = Seq(ButtonType.OK, ButtonType.Cancel)
    dialog.dialogPane().content = grid

    dialog.resultConverter = dialogButton =>
      if (dialogButton == ButtonType.OK) {
        for (i <- checkBoxes.indices)
          cells(i) = checkBoxes(i).selected.value
        cells
      }
      else null

    val result = dialog.showAndWait()
    result match {
      case Some(_) =>
        for(cell <- cells)
          println(cell + " ")

      case None => println("Dialog returned: None")
    }
  }


}
